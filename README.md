# yaml-tool-frontend

## Getting started

First we need to install the dependencies with

```
npm install
```

Then we can run the application by using 

```
npm run dev
```

After that you can check the application running locally in the browser 
